import streamlit as st
from PIL import Image
import datetime
import pandas as pd
import numpy as np

def main():
    st.title("Hello World")
    st.text("This is a simple text")
    st.markdown("This is a **markdown** text")
    st.markdown("**This is a markdown text**")
    st.header("This is a header")
    st.subheader("This is a subheader")
    st.success("Successfull")
    st.info("Information")
    st.warning("Warning")
    st.error("Error")
    st.help(range)
    st.write("Writing example text with write function")
    img = Image.open('business-cat-working.gif')
    st.image(img, caption='This is a caption', use_column_width=True)
    # my_video = open('video_name', 'rb')
    # st.video(my_video)
    st.video('https://www.youtube.com/watch?v=DHfRfU3XUEo')

    st.checkbox("Hide and Seek")
    if st.checkbox("Show/Hide"):
        st.text("Showing text")

    status = st.radio("What is your favorite color?", ("Red", "Green", "Blue"))
    if status == "Red":
        st.text("You picked Red")
    elif status == "Green":
        st.text("You picked Green")
    else:
        st.text("You picked Blue")

    st.button("unnecessary button")
    if st.button("Press me"):
        st.text("You pressed the button")

    selected_number = st.selectbox("Select a number", [0, 1, 2, 3, 4, 5])
    if selected_number != 0:
        st.text(f"I am sending you {selected_number} cats")
    else:
        st.text(f"No cats for you -.-")

    multi_select = st.multiselect("Select multiple numbers", [0, 1, 2, 3, 4, 5])
    if len(multi_select) > 0:
        st.text(f"You selected {multi_select}")
    else:
        st.text("You didn't select anything")

    options = st.slider("Select a number", 0.0, 1.0) # Step size is 0.1 (optional)
    # options = st.slider("Select a number", 0.0, 1.0, 0.5) # Initial value is 0.5 (optional)
    # options = st.slider("Select a number", 0.0, 1.0, 0.5, 0.1) # Step size is 0.1 (optional)

    user_input = st.text_input("Enter your name")
    if st.button("Submit"):
        st.text(f"Hello, {user_input.title()}")

    message = st.text_area("Enter a message:", "...your message...")
    if st.button("Submit message"):
        st.text(f"Hello, {user_input}")

    today = st.date_input("Today's date", datetime.datetime.now())
    input_date = st.date_input("Enter a date", datetime.datetime(2020, 1, 1))
    input_time = st.time_input("Enter a time", datetime.time(12, 0))

    #raw data
    st.write("This is a raw data")
    st.text("This is a raw text")
    st.code("import pandas as pd")

    #multiple line code
    with st.echo():
        import pandas as pd
        import seaborn as sns
        import matplotlib.pyplot as plt
        df = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]})
        df
    #sidebar
    st.sidebar.header("This is a sidebar")
    st.sidebar.title("This is a sidebar title")
    st.sidebar.text("This is a sidebar text")
    st.sidebar.markdown("This is a sidebar markdown text")
    st.sidebar.subheader("This is a sidebar subheader")

    df = pd.read_csv('Mall_Customers.csv')
    st.table(df.head())
    st.write(df.head())
    st.dataframe(df.head())

    x = st.slider('x')
    srs = pd.Series((np.random.randn(x)))
    st.line_chart(srs.values)

    option = st.selectbox(
    'Which number do you like best?',
     srs)

if __name__ == "__main__":
    main()
