import streamlit as st
import pandas as pd
import numpy as np
import datetime
from PIL import Image

st.title('Hello World')
st.text('This is a simple text with the text function')
st.markdown('`This is a markdown text`')
st.markdown('We can do a lot of interesting ***stuff*** with markdown')
st.header('This is a header')
st.subheader('Subheader hurra!')

st.success('Congrats!')
st.info('Warning!')
st.error('Dang!')

st.help('asd')
st.write('This is a text written with write function')

img = Image.open('business-cat-working.gif')

st.image(img, caption='Cattie', use_column_width=True)

st.video('https://www.youtube.com/watch?v=DHfRfU3XUEo')

#Checkbox
if st.checkbox('Hide and Seek'):
	st.write('Seek')

status = st.radio('What is your favourite colour?', ('Red', 'Green', 'Blue'))

st.write(f'Your favourite colour is {status}')

#Button
if st.button('Unnecessary button'):
	st.success('big brain move')

#Selectbox
selected_number = st.selectbox('Select a number', [0, 1, 2, 3, 4, 5])
if selected_number == 0:
	st.write('No cats for you -.-')
else:
	st.write(f'I am sending you {selected_number} cats')


multi_select = st.multiselect('Select multiple numbers', [0, 1, 2, 3, 4, 5])

if len(multi_select) > 0:
	st.write(f'you selected {multi_select}')
else:
	st.write('You didnt select anything')


options = st.slider('Select a number', 0.0, 5.0, 3.0, 0.1)


name = st.text_input('Enter your name')

if st.button('Submit'):
	st.write(f'Hello, {name.title()}')

txt = st.text_area('Enter a message', placeholder='Type right here...')


st.date_input('Date', datetime.datetime.now())
st.time_input('Time', datetime.time(12,0))

st.code('import pandas as pd \nimport numpy as np')

with st.echo():
	import numpy as np
	import pandas as pd
	import matplotlib.pyplot as plt
	df = pd.DataFrame({'a':[1, 2, 3], 'b':[4, 5, 6]})
	df


st.sidebar.write('This will show on sidebar')
st.sidebar.title('Sidebar title')
st.sidebar.slider('input', 0, 5, 1, 1)
st.sidebar.markdown('## Markdown header')


df = pd.read_csv('Mall_Customers.csv')

st.table(df.head())
st.write(df.head())
st.dataframe(df.head())

x = st.slider('x')

st.line_chart(df['Age'][:x])
